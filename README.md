[TOC]

# Overview

Promise/A+ based NPM module - adapted version of [`sTORAGEmANAGER_ParseRestAPI.js`](https://bitbucket.org/gwagner57/ontojs/src/badaa61994396729f101696f55b5249ef78e4c5c/src/sTORAGEmANAGER_ParseRestAPI.js) written by the chair of Prof. Gerd Wagner, BTU Cottbus

You can find the source code [here](https://bitbucket.org/workslon/storagemanager_parseapi/src/59a515f4edacd9953884021037d3cf142ee3ec7b/index.js)

# Dependencies

Requires and extends [sTORAGEmANAGER.js](https://bitbucket.org/workslon/storagemanager)

# Usage

ParseRestAPI storage manager adapter is a part of [sTORAGEmANAGER.js](https://bitbucket.org/workslon/storagemanager). It extends its default functionality and provides with Parse API instead of default LocalStorage API.

So, to use it you need to install [sTORAGEmANAGER.js](https://bitbucket.org/workslon/storagemanager) and configure it to use ParseRestAPI adapter. See examples below.

__NOTE:__ as [Parse](http://parse.com) team has open-sourced it server there is no longer need to provide REST API key header in the requests

## CommonJS

```javascript
var sTORAGEmANAGER = require('storage-manager');
var parseStorageManager = new sTORAGEmANAGER({
  name: 'ParseRestAPI',
  appId: 'AppId',
  apiUrl: 'https://my-app.com/api/classes'
});

// callback-based usage
parseStorageManager
  .retrieve(Book, 'XXX', function (error, result) {
    if (error) {
      console.log(error);
    } else {
      console.log(result);
    }
  });

// Promise-based usage
parseStorageManager
  .retrieve(Book, 'XXX')
  .then(function (result) {
    console.log(result);
  }, function (error) {
    console.log(error);
  })
```

## ES6 Modules

```javascript
import sTORAGEmANAGER from 'storage-manager';
const parseStorageManager = new sTORAGEmANAGER({
  name: 'ParseRestAPI',
  appId: 'AppId',
  apiUrl: 'https://my-app.com/api/classes'
})
```

# API References

## Methods

__.retrieve(modelClass, objectId, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | Id of the target object in database           |
| continueProcessing | Function | Optional callback (since you can use `.then`) |

__.retrieveAll(modelClass, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| continueProcessing | Function | Optional callback (since you can use `.then`) |

__.add(modelClass, data, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| data               | Object   | Object to be saved to the database            |
| continueProcessing | Function | Optional callback (since you can use `.then`) |

__.update(modelClass, objectId, data, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | ID of the object to be updated                |
| data               | Object   | New data to be saved                          |
| continueProcessing | Function | Optional callback (since you can use `.then`) |

__.destroy(modelClass, objectId, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | ID of the object to be destroyed              |
| continueProcessing | Function | Optional callback (since you can use `.then`) |